local keymap = vim.keymap.set

-- Remap for German Layout
keymap("", "ö", ":", { desc = 'German Colon Remap' })

-- Change tab size
keymap('n', '<leader>ts', function()
    vim.ui.input({ prompt = 'Update tab size: ' }, function(input)
        vim.opt_local.tabstop = tonumber(input)
        vim.opt_local.softtabstop = tonumber(input)
        vim.opt_local.shiftwidth = tonumber(input)
    end)
end)

-- General Vim Things
keymap("n", "<leader>wq", function()
    vim.cmd("Neotree close")
    vim.cmd("wqa")
end, { noremap = true, silent = true, desc = 'Quit' })
keymap("n", "<leader>ul", "<cmd>Lazy<cr>", { noremap = true, silent = true, desc = 'Lazy' })

-- Finding Stuff

-- Buffer Related Keymaps
keymap("n", "<leader>bd", "<cmd>bd<cr>", { noremap = true, silent = true, desc = 'Close Buffer' })
keymap("n", "<leader>bk", "<cmd>%bd|e#<cr>", { noremap = true, silent = true, desc = 'Close all other Buffers' })
keymap("n", "<leader>bf", function() vim.lsp.buf.format() end, { noremap = true, silent = true, desc = 'Format Buffer' })

-- LSP Keymaps
keymap("n", "<leader>ld", function() vim.lsp.buf.definition() end,
    { noremap = true, silent = true, desc = 'Go To Definition' })
keymap("n", "<leader>ls", "<cmd>Pick lsp scope='document_symbol'<cr>",
    { noremap = true, silent = true, desc = 'Show all Symbols' })
keymap("n", "<leader>lr", function() vim.lsp.buf.rename() end, { noremap = true, silent = true, desc = 'Rename This' })
keymap("n", "<leader>la", function() vim.lsp.buf.code_action() end,
    { noremap = true, silent = true, desc = 'Code Actions' })

-- UI Related Keymaps
-- Window Navigation
keymap("n", "<leader>wl", "<cmd>wincmd l<cr>", { noremap = true, silent = true, desc = 'Focus Left' })
keymap("n", "<leader>wk", "<cmd>wincmd k<cr>", { noremap = true, silent = true, desc = 'Focus Up' })
keymap("n", "<leader>wj", "<cmd>wincmd j<cr>", { noremap = true, silent = true, desc = 'Focus Down' })
keymap("n", "<leader>wh", "<cmd>wincmd h<cr>", { noremap = true, silent = true, desc = 'Focus Right' })

keymap('n', '<S-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
keymap('n', '<S-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
keymap('n', '<S-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
keymap('n', '<S-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

keymap("n", "<leader>wq", "<cmd>wincmd q<cr>", { noremap = true, silent = true, desc = 'Close Window' })
keymap("n", "<leader>k", "<C-^>", { noremap = true, silent = true, desc = "Alternate buffers" })

-- Change Colorscheme
keymap("n", "<leader>ud", "<cmd>set background=dark<cr>", { noremap = true, silent = true, desc = 'Dark Background' })
keymap("n", "<leader>ub", "<cmd>set background=light<cr>", { noremap = true, silent = true, desc = 'Light Backround' })

-- Custom Functionality
-- Password Generator
keymap("n", "<leader>ip",
    function()
        local command = 'pwgen -N 1 32'
        local password = vim.fn.systemlist(command)
        for _, line in ipairs(password) do
            vim.api.nvim_put({ line }, '', true, true)
        end
    end,
    { noremap = true, silent = true, desc = 'Insert Password' })
