local autocmd = vim.api.nvim_create_autocmd

-- Open Neotree after loading a Session
autocmd("SessionLoadPost", {nested = true, callback = function() vim.cmd("Neotree filesystem") end})

-- Mattern
autocmd({ "BufWinEnter", "BufEnter" }, { callback = function() require("mattern").mattern_print() end })

-- Flash on Yank
autocmd({"TextYankPost"}, { callback = function () require('vim.highlight').on_yank() end})
