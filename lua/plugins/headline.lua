return {
    {
        enabled = true,
        lazy = true,
        "Bekaboo/dropbar.nvim",
        -- optional, but required for fuzzy finder support
        dependencies = {
            "nvim-telescope/telescope-fzf-native.nvim"
        }
    }
}
