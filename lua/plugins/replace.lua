return {
    {
        'AckslD/muren.nvim',
        opts = {},
        keys = {
            { "<leader>rm", "<cmd>MurenOpen<cr>", desc = "Toggle Muren" },
        },
    },
}
