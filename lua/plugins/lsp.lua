return {
    {
        'mfussenegger/nvim-lint',
        config = function()
            require('lint').linters_by_ft = {
              yaml = {'ansible_lint',}
            }
            -- require('lint').linters.ansible_lint.args = {
            --     '-c',
            --     '/home/dosa/.config/ansible-lint.yml'
            -- }
        end
    },
    {
        'neovim/nvim-lspconfig',
        event = { "BufReadPost", "BufNewFile", "BufWritePre" },
        cmd = { 'LspInfo', 'LspInstall', 'LspStart' },
        dependencies = {
            -- { 'hrsh7th/cmp-nvim-lsp' },
            { 'williamboman/mason-lspconfig.nvim', config = function() end },
            { 'williamboman/mason.nvim',           config = true },
        },
        config = function()
            require('lspconfig').lua_ls.setup {
                settings = {
                    Lua = {
                        diagnostics = {
                            disable = { "lowercase-global", "undefined-global" }
                        },
                        hint = {
                            enable = true,
                            arrayIndex = "Auto",
                            await = true,
                            paramName = "All",
                            paramType = true,
                            semicolon = "SameLine",
                            setType = false,
                        },
                    },
                },
            }
            require('lspconfig').sqlls.setup {}
            require('lspconfig').bashls.setup {}
            require('lspconfig').ansiblels.setup {
                settings = {
                    {
                        ansible = {
                            ansible = {
                                path = 'ansible'
                            },
                            executionEnvironment = {
                                enabled = false
                            },
                            python = {
                                interpreterPath = 'python'
                            },
                            validation = {
                                enabled = true,
                                lint = {
                                    enabled = true,
                                    path = 'ansible-lint',
                                    arguments = '-c /home/dosa/.config/ansible-lint.yml'
                                }
                            }
                        }
                    }
                }
            }
        end,
    },
}
