return {
    {
        "akinsho/bufferline.nvim",
        enabled = true,
        event = "VeryLazy",
        -- branch = "main",
        -- commit = "f6f00d9ac1a51483ac78418f9e63126119a70709",
        dependencies = "echasnovski/mini.icons",
        config = function()
            require("bufferline").setup {
                options = {
                    offsets = {
                        {
                            filetype = "neo-tree",
                            text = "NeoVIM",
                            highlight = "BufferLineHintSelected",
                            text_align = "center",
                        },
                    },
                }
            }
        end,
        keys = {
            { "<C-h>", "<cmd>BufferLineCyclePrev<cr>", desc = "Previous Buffer" },
            { "<C-l>", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
        }
    },
    {
        "ramilito/winbar.nvim",
        event = "BufReadPre", -- Alternatively, BufReadPre if we don"t care about the empty file when starting with "nvim"
        dependencies = { "echasnovski/mini.icons" },
        config = function()
            require("winbar").setup({
                -- your configuration comes here, for example:
                icons = true,
                diagnostics = true,
                buf_modified = true
            })
        end
    },
}
