return {
    {
        'folke/tokyonight.nvim',
        lazy = true,
        opts = {
            day_brightness = 0.1,
            lualine_bold = true
        },
        init = function()
            vim.cmd('colorscheme tokyonight')
        end
    },
    {
        "catppuccin/nvim",
        name = "catppuccin",
        lazy = true,
        config = function()
            require("catppuccin").setup({
                flavour = "macchiato",
                dim_inactive = {
                    enabled = true, -- dims the background color of inactive window
                    shade = "dark",
                    percentage = 0.5, -- percentage of the shade to apply to the inactive window
                },
                integrations = {
                    gitsigns = true,
                    mason = true,
                    neotree = true,
                    cmp = true,
                    treesitter = true,
                    which_key = true,
                },
            })
        end,
    },
    {
        "miikanissi/modus-themes.nvim",
        lazy = true,
        opts = {
            variant = "tinted"
        },
    }
}
