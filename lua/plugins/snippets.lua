return {
    {
        "L3MON4D3/LuaSnip",
        -- follow latest release.
        version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
        keys = {
            { "<C-K>", function() require('luasnip').expand() end },
            { "<C-L>", function() require('luasnip').jump(1) end },
            { "<C-J>", function() require('luasnip').jump(-1) end },
            { "<C-E>", function()
                if require('luasnip').choice_active() then
                    require('luasnip').change_choice(1)
                end
            end },
        },
        config = function()
            require("luasnip.loaders.from_snipmate").load({ paths = vim.fn.stdpath("config") .. "/snippets" })
        end
    },
}
