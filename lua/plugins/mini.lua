return {
    {
        "echasnovski/mini.icons",
        opts = {},
        lazy = true,
        specs = {
            { "nvim-tree/nvim-web-devicons", enabled = false, optional = true },
        },
        init = function()
            package.preload["nvim-web-devicons"] = function()
                -- needed since it will be false when loading and mini will fail
                package.loaded["nvim-web-devicons"] = {}
                require("mini.icons").mock_nvim_web_devicons()
                return package.loaded["nvim-web-devicons"]
            end
        end,
    },
    {
        "echasnovski/mini.nvim",
        event = "BufEnter",
        keys = {
            { "<leader>e",  function() require("mini.files").open(vim.api.nvim_buf_get_name(0)) end, desc = "Find Manualy" },
            { "<leader>j",  function() require("mini.splitjoin").toggle() end,                       desc = "Splitjoin Toggle" },
            { "<leader>mf", function() require("mini.pick").builtin.files() end,                     desc = "Mini Find Files" },
            { "<leader>mg", function() require("mini.pick").builtin.grep_live() end,                 desc = "Mini Search in Files" },
            { "<leader>mb", function() require("mini.pick").builtin.buffers() end,                   desc = "Mini Switch Buffer" },
        },
        config = function()
            require("mini.ai").setup()
            require("mini.basics").setup({
                options = {
                    extra_ui = true,
                    win_borders = "double",
                },
                mappings = {
                    windows = true,
                }
            })
            require("mini.bracketed").setup()
            require("mini.bufremove").setup()
            require("mini.comment").setup()
            require("mini.extra").setup()
            require("mini.files").setup({
                windows = {
                    preview = true,
                    width_preview = 80,
                }
            })
            require("mini.hipatterns").setup()
            require("mini.indentscope").setup({
                draw = { animation = function() return 1 end, },
                symbol = "│"
            })
            require("mini.move").setup()
            require("mini.operators").setup()
            local win_config = function()
                height = math.floor(0.618 * vim.o.lines)
                width = math.floor(0.3 * vim.o.columns)
                return {
                    anchor = "NW",
                    height = height,
                    width = width,
                    row = 0,
                    col = math.floor(0.5 * (vim.o.columns - width)),
                }
            end
            require("mini.pick").setup({
                window = {
                    -- Float window config (table or callable returning it)
                    config = win_config,
                    prompt_cursor = "▏",
                    prompt_prefix = " ",
                },
            })
            require("mini.splitjoin").setup()
            -- require("mini.starter").setup({
            --     items = {
            --         require("mini.starter").sections.builtin_actions(),
            --         require("mini.starter").sections.recent_files(5, false),
            --         require("mini.starter").sections.recent_files(5, true),
            --         { name = "Lazy", action = "Lazy", section = "Lazy" },
            --     },
            --     header = [[
            --     ███╗   ███╗██╗   ██╗██╗███╗   ███╗
            --     ████╗ ████║██║   ██║██║████╗ ████║
            --     ██╔████╔██║██║   ██║██║██╔████╔██║
            --     ██║╚██╔╝██║╚██╗ ██╔╝██║██║╚██╔╝██║
            --     ██║ ╚═╝ ██║ ╚████╔╝ ██║██║ ╚═╝ ██║
            --     ██║     ██║  ╚═══╝  ╚═╝██║     ██║
            --     ██║     ██║ini      nvi██║     ██║
            --     ╚═╝     ╚═╝            ╚═╝     ╚═╝]],
            --     footer = function()
            --         local stats = require("lazy.stats").stats()
            --         local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
            --         return "Startup Time: " .. ms .. " ms"
            --     end
            -- })
            -- require("mini.statusline").setup({
            --     use_icons = true,
            -- })
            local animate = require("mini.animate")
            animate.setup {
                scroll = {
                    -- Disable Scroll Animations, as the can interfer with mouse Scrolling
                    enable = false,
                },
                cursor = {
                    timing = animate.gen_timing.cubic({ duration = 50, unit = "total" })
                },
            }
            require("mini.surround").setup()
            -- require("mini.tabline").setup()
            -- require("mini.test").setup()
            -- require("mini.trailspace").setup()
            require("mini.visits").setup()
        end
    },
}
