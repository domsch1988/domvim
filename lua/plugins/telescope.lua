return {
    "nvim-telescope/telescope.nvim",
    enabled = true,
    lazy = true,
    dependencies = {
        "nvim-lua/plenary.nvim",
        {
            "nvim-telescope/telescope-fzf-native.nvim",
            build =
            "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
            -- Uncomment this on Linux
            -- build = "make"
            cond = function()
                return vim.fn.executable "cmake" == 1
            end,
        },
    },
    opts = {
    },
    init = function()
        require("telescope").load_extension("fzf")
    end,
    keys = {
    }
}
