return {
    {
        'domsch1988/mattern.nvim',
        event = "BufReadPre",
        config = function()
            require('mattern').setup({
                position = "eol",
                markers = {
                    { [[acs:]], 'Matched ACS', "@comment.error", "lua" },
                    { [[base_pw:]], 'base_pw is deprecated! Replace with root_pw', "@comment.error"},
                    { [[asgard_backup_path:]], 'Rollout New Backup!', "@comment.error", "yaml" },
                    { [[evd_backup_path:]], 'Rollout New Backup!', "@comment.error", "yaml" },
                    { [[CONFIG__DB_EVD_HDD_ABSOLUTE:]], 'Default: 200000', "@text.note" },
                }
            })
        end
    }
}
