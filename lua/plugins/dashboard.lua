local function get_quote()
    local found_curl, curl = pcall(require, "plenary.curl")
    if not found_curl then
        error("plenary not found")
        return ""
    end

    local response = curl.get("https://zenquotes.io/api/random", {
        headers = {
            ["User-Agent"] = "curl/7.68.0",
        },
    })

    if response.status ~= 200 then
        error("Http failed with " .. response.status, 1)
        return ""
    end

    local json_data = vim.json.decode(response.body, {})
    if json_data == {} or json_data == nil then
        error("empty json from quotes API decoded")
        return ""
    end

    return json_data[1].q
end

return {
    {
        'nvimdev/dashboard-nvim',
        event = 'VimEnter',
        opts = {
            theme = 'hyper',
            config = {
                shortcut = {
                },
                packages = { enable = true }, -- show how many plugins neovim loaded
                project = { enable = true, limit = 8, icon = ' ', label = '', action = 'Telescope find_files cwd=' },
                mru = { limit = 10, icon = ' ', label = '', cwd_only = false },
                footer = {get_quote()}, -- footer
            }
        },
        dependencies = { { 'nvim-tree/nvim-web-devicons' } }
    }
}
