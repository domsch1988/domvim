return {
    {
        "ibhagwan/fzf-lua",
        lazy = true,
        -- optional for icon support
        dependencies = { "echasnovski/mini.icons" }, -- not strictly required, but recommended
        config = function()
            -- calling `setup` is optional for customization
            require("fzf-lua").setup(
                {
                    'default-title',
                    fzf_colors = true,
                    defaults = {
                        formatter = 'path.filename_first',
                        winopts = {
                            preview = {
                                default = 'bat',
                                flip_columns = 200
                            }
                        },
                    },
                }
            )
        end,
        keys = {
            { "<leader><space>", function() require('fzf-lua').buffers() end,              desc = "Find Buffer" },
            { "<leader>fs",      function() require('fzf-lua').files() end,                desc = "Find Files" },
            { "<leader>fo",      function() require('fzf-lua').oldfiles() end,             desc = "Find Last Files" },
            { "<leader>ff",      function() require('fzf-lua').live_grep_native() end,     desc = "Grep" },
            { "<leader>fg",      function() require('fzf-lua').live_grep_resume() end,     desc = "Re-Grep" },
            { "<leader>fh",      function() require('fzf-lua').help_tags() end,            desc = "Find Help" },
            { "<leader>fw",      function() require('fzf-lua').grep_cword() end,           desc = "Grep Word" },
            { "<leader>fW",      function() require('fzf-lua').grep_cWORD() end,           desc = "Grep WORD" },
            { "<leader>gc",      function() require('fzf-lua').git_bcommits() end,         desc = "Git Commits (Buffer)" },
            { "<leader>gs",      function() require('fzf-lua').git_status() end,           desc = "Git Status" },
            { "<leader>gb",      function() require('fzf-lua').git_branches() end,         desc = "Git Branches" },
            { "<A-x>",           function() require('fzf-lua').commands() end,             desc = "Command Palette" },
            { "<C-p>",           function() require('fzf-lua').commands() end,             desc = "Command Palette" },
            { ",",               function() require('fzf-lua').blines() end,               desc = "Buffer Lines" },
            { "<leader>fa",      function() require('fzf-lua').builtin() end,              desc = "fzf Pickers" },
            { "<leader>fc",      function() require('fzf-lua').colorschemes() end,         desc = "Change Colorscheme" },
            { "<leader>fC",      function() require('fzf-lua').awesome_colorschemes() end, desc = "Change Colorscheme (Online)" },
        }
    }
}
