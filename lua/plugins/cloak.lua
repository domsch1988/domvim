return {
    {
        'laytan/cloak.nvim',
        keys = {
            { "<leader>up", "<cmd>CloakToggle<cr>", desc = "Toggle Password Cloaking" }
        },
        ft = {
            'yaml'
        },
        config = function()
            require('cloak').setup({
                enabled = true,
                cloak_character = '*',
                highlight_group = 'Comment',
                cloak_length = nil,
                try_all_patterns = true,
                patterns = {
                    {
                        file_pattern = {
                            '.env*',
                            '*.yml',
                            '*',
                        },
                        cloak_pattern = {
                            { '(pw: ).+',           replace = '%1' },
                            { '(pass: ).+',         replace = '%1' },
                            { '(passwd: ).+',       replace = '%1' },
                            { '(password: ).+',     replace = '%1' },
                            { '(password_usr: ).+', replace = '%1' },
                        },
                        -- A function, table or string to generate the replacement.
                        -- The actual replacement will contain the 'cloak_character'
                        -- where it doesn't cover the original text.
                        -- If left emtpy the legacy behavior of keeping the first character is retained.
                        replace = nil,
                    },
                },
            })
        end
    },
}
