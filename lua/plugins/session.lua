return {
    {
        'Shatur/neovim-session-manager',
        dependencies = {
            'nvim-neo-tree/neo-tree.nvim',
            'stevearc/dressing.nvim'
        },
        opts = {},
        keys = {
            { "<leader>sl", "<cmd>SessionManager load_session<cr>",         desc = "Load Session" },
            { "<leader>ss", "<cmd>SessionManager save_current_session<cr>", desc = "Save Session" }
        },
    },
}
