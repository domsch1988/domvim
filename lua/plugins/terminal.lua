return {
    {
        'akinsho/toggleterm.nvim',
        keys = {
            { "<leader>tt", "<cmd>ToggleTerm<cr>", desc = "ToggleTerm"},
        },
        opts = {}
    }
}
