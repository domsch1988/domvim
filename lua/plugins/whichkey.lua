return {
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
            win = {
                no_overlap = true,
                width = 90,
                border = "rounded",
                padding = { 1, 1 }, -- extra window padding [top/bottom, right/left]
            },
            layout = {
                width = { min = 20, max = 30 }, -- min and max width of the columns
                spacing = 3,                    -- spacing between columns
                align = "left",                 -- align columns left, center or right
            },
            spec = {
                mode = { "n", "v" },
                { "<leader>b", group = "Buffer" },
                { "<leader>f", group = "Find" },
                { "<leader>g", group = "Git" },
                { "<leader>h", group = "Hunk" },
                { "<leader>i", group = "Insert" },
                { "<leader>l", group = "LSP" },
                { "<leader>m", group = "mini.nvim" },
                { "<leader>n", group = "Neotree" },
                { "<leader>r", group = "Replace" },
                { "<leader>s", group = "Session" },
                { "<leader>t", group = "Toggle" },
                { "<leader>u", group = "UI" },
                { "<leader>w", group = "Window" },
            }
        },
        config = function(_, opts)
            local wk = require("which-key")
            wk.setup(opts)
        end,
    }
}
