" Vim syntax file
" Language:     Asterisk ael config file
" Maintainer:   BeF
" Last Change:  2006 Jan 31
" version 0.1
"

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

"testing only
syn sync clear
syn sync fromstart

syn keyword     asteriskTodo    TODO contained
syn match       asteriskComment "//.*" contains=asteriskTodo
 
"syn match      asteriskTop     "^\s*[[:alpha:]]\s+.*\s*{" contains=asteriskTopKeywords
syn match       asteriskTop     "^\s*[[:alpha:]]\s*" contains=asteriskTopKeywords
syn keyword     asteriskTopKeywords     context globals macro contained

syn match       asteriskDialplanSpecial "=>"    contained
syn match       asteriskExten   "\s*[^, ]\+\s*=>" contains=asteriskDialplanSpecial,asteriskPattern contained
syn match       asteriskApplication     "[[[:alnum:]_]\+(.*);" contains=asteriskArgumentSpecial,asteriskVar contained
syn match       asteriskArgumentSpecial "\(|\|&\)" contained
syn match       asteriskFunction        "[[[:alnum:]_]\+(.*)" contains=asteriskFunctionNames,asteriskVar,asteriskFunction contained
syn keyword     asteriskFunctionNames   URIENCODE URIDECODE TXTCIDNAME ENUMLOOKUP CALLERID CURL SORT CUT VMCOUNT QUEUEAGENTCOUNT IAXPEER CHECKSIPDOMAIN SIPCHANINFO SIPPEER SIP_HEADER MUSICCLASS LANGUAGE TIMEOUT DB_EXISTS DB ENV IFTIME IF EXISTS SET ISNULL CDR EVAL STRFTIME LEN REGEX FIELDQTY GROUP_LIST GROUP GROUP_MATCH_COUNT GROUP_COUNT MATH CHECK_MD5 MD5 DUNDILOOKUP contained
syn region      asteriskDialplan        start="{" end="};" contains=asteriskDialplan,asteriskComment,asteriskVar,asteriskExten,asteriskApplication,asteriskFunction




                
                
""              
syn match       asteriskPattern         "_\(\[[[:alnum:]#*\-]\+\]\|[[:alnum:]#*]\)*\.\?" contained
syn match       asteriskPattern         "[^A-Za-z0-9,]\zs[[:alnum:]#*]\+\ze" contained
syn match       asteriskVar             "\${_\{0,2}[[:alpha:]][[:alnum:]_]*\(:-\?[[:digit:]]\+\(:[[:digit:]]\+\)\?\)\?}"
syn match       asteriskVar             "_\{0,2}[[:alpha:]][[:alnum:]_]*\ze="
syn match       asteriskVarLen          "\${_\{0,2}[[:alpha:]][[:alnum:]_]*(.*)}" contains=asteriskVar,asteriskVarLen,asteriskExp
syn match       asteriskVarLen          "(\zs[[:alpha:]][[:alnum:]_]*(.\{-})\ze=" contains=asteriskVar,asteriskVarLen,asteriskExp
syn match       asteriskExp             "\$\[.\{-}\]" contains=asteriskVar,asteriskVarLen,asteriskExp



" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
:if version >= 508 || !exists("did_conf_syntax_inits")
  if version < 508
    let did_conf_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif


  HiLink        asteriskComment Comment
  HiLink        asteriskTodo Todo
  HiLink        asteriskVar Identifier
  HiLink        asteriskTopKeywords Type
  HiLink        asteriskDialplanSpecial Special
  HiLink        asteriskArgumentSpecial Special
  HiLink        asteriskFunctionNames Function
  HiLink        asteriskPattern         String
 delcommand HiLink
endif


let b:current_syntax = "asterisk-ael"

" vim: ts=8 sw=2
