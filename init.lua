local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end

vim.opt.rtp:prepend(lazypath)

-- WSL Paste Workaround
-- Strip newline character
-- TODO: make this work with System Clipboard
--
-- vim.g.clipboard = {
--     name = 'wayland-strip-carriage',
--     copy = {
--         ['+'] = 'wl-copy --foreground --type text/plain',
--         ['*'] = 'wl-copy --foreground --type text/plain --primary',
--     },
--     paste = {
--         ['+'] = function()
--             return vim.fn.systemlist('wl-paste --no-newline | tr -d "\r"')
--         end,
--         ['*'] = function()
--             return vim.fn.systemlist('wl-paste --no-newline --primary | tr -d "\r"')
--         end,
--     },
--     cache_enabled = 1,
-- }

vim.g.mapleader = " "
vim.o.background = "light"
vim.opt.listchars = { extends = '.', precedes = '|', nbsp = '_', tab = '└─┘' }
vim.opt.smartindent = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 8
vim.opt.expandtab = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 10
vim.opt.clipboard = "unnamedplus"
vim.opt.fileformat = "unix"
vim.opt.laststatus = 3
-- vim.opt.splitkeep = "screen"
vim.opt.equalalways = true
vim.opt.splitright = true
vim.opt.fillchars:append('eob: ')

require("lazy").setup({
    spec = {
        { import = "plugins" },
    },
    defaults = {
        -- By default, only LazyVim plugins will be lazy-loaded. Your custom plugins will load during startup.
        -- If you know what you're doing, you can set this to `true` to have all your custom plugins lazy-loaded by default.
        lazy = true,
        -- It's recommended to leave version=false for now, since a lot the plugin that support versioning,
        -- have outdated releases, which may break your Neovim install.
        version = false, -- always use the latest git commit
        -- version = "*", -- try installing the latest stable version for plugins that support semver
    },
    install = {},
    checker = { enabled = true }, -- automatically check for plugin updates
    change_detection = {
        enabled = false,
    },
})

require("autocmds")
require("filetypes")
require("highlights")
require("keybinds")
require("custom")
